

import 'package:flutter/material.dart';
import 'package:flutter_study/platform_channel/android/battery_channel.dart';

class BatteryRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return BatteryRouteState();
  }
}

class BatteryRouteState extends State<BatteryRoute> {
  String _batteryLevel = 'Unknown battery level.';

  getBatteryLevel() async{
    _batteryLevel = await BatteryChannel.getBatteryLevel();
    setState(() {});
  }

  getBatteryLevelWithThen() {
    _batteryLevel =  BatteryChannel.getBatteryLevelWithThen();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    BatteryChannel.initChannels();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BatteryRoute"),
        centerTitle: true,
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            new ElevatedButton(
              child: new Text('Get Battery Level'),
              onPressed: (){
                getBatteryLevel();
              },
            ),
            new Text(_batteryLevel),
          ],
        ),
      ),
    );
  }
}