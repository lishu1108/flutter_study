import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';

class BleRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return BleRouteState();
  }
}

class BleRouteState extends State<BleRoute> {
  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ble 蓝牙"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextButton(
            child: Text("检查蓝牙权限"),
            onPressed: () async{
              var status = await Permission.location.status;
              var status2 = await Permission.bluetoothScan.status;
              var status3 = await Permission.bluetoothConnect.status;
              var status4 = await Permission.bluetoothAdvertise.status;
              if(status.isDenied || status2.isDenied || status3.isDenied || status4.isDenied) {
                Fluttertoast.showToast(
                  msg: "蓝牙权限未开启",
                );
                Map<Permission, PermissionStatus> statuses = await [
                  Permission.location,
                  Permission.bluetoothScan,
                  Permission.bluetoothConnect,
                  Permission.bluetoothAdvertise
                ].request();

                statuses.forEach((key, value) {
                  if(value.isDenied) {
                    Fluttertoast.showToast(
                      msg: "${key.value}权限被拒绝",
                    );
                  }
                });

              } else {
                var bleDisabled = await Permission.bluetooth.serviceStatus.isDisabled;
                if(bleDisabled) {
                  Fluttertoast.showToast(
                    msg: "请打开蓝牙",
                  );
                  // if(Platform.isAndroid) {
                  //   // 该方法会根据 android 的版本去动态申请所需要的权限
                  //   await FlutterBluePlus.turnOn();
                  // } else if(Platform.isIOS) {
                  //
                  // }
                } else {
                  Fluttertoast.showToast(
                    msg: "蓝牙已打开",
                  );
                }
              }
            },
          ),
          TextButton(
            child: Text("扫描蓝牙"),
            onPressed: () async {
              var subscription = FlutterBluePlus.scanResults.listen((results) {
                for (ScanResult r in results) {
                  print('${r.device.localName} found! rssi: ${r.rssi}');
                }
              });

              FlutterBluePlus.startScan(timeout: Duration(seconds: 15),androidUsesFineLocation: false);
            },
          ),
          TextButton(
            child: Text("停止扫描"),
            onPressed: () async{
              await FlutterBluePlus.stopScan();
            },
          ),
          SizedBox(
            height: 400,
            child: ListView.builder(
              physics: ClampingScrollPhysics(),
              itemExtent: 50,
              itemCount: 0,
              itemBuilder: (ctx, index){
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(""),
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    FlutterBluePlus.stopScan();
    super.dispose();
  }
}