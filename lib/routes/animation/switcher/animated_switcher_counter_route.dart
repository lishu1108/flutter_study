

import 'package:flutter/material.dart';

/// 通用 "动画切换组件"
class AnimatedSwitcherCounterRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new AnimatedSwitcherCounterRouteState();
  }

}

class AnimatedSwitcherCounterRouteState extends State<AnimatedSwitcherCounterRoute> {
  int count = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("通用动画切换组件"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AnimatedSwitcher(
              duration: const Duration(milliseconds: 500),
              transitionBuilder: (child, animation){
                // 执行缩放动画
                return ScaleTransition(child: child, scale: animation);
              },
              child: Text(
                '$count',
                // 显示指定 key，不同的 key 会被认为是不同的 text，这样才能执行动画
                key: ValueKey<int>(count),
              ),
            ),
            TextButton(
              child: const Text("+1"),
              onPressed: (){
                setState(() {
                  count += 1;
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}