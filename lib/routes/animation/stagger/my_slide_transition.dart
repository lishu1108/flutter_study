
import 'package:flutter/material.dart';

class MySlideTransition extends AnimatedWidget {

  final bool transformHiTests;
  final Widget child;
  final Animation<Offset> position;

  MySlideTransition({
    Key? key,
    required this.position,
    this.transformHiTests = true,
    required this.child,
  }): super(key: key, listenable: position);

  @override
  Widget build(BuildContext context) {
    Offset offset = position.value;
    // 动画反向执行时，调整 x 偏移，实现从"左边滑出隐藏"
    if(position.status == AnimationStatus.reverse){
      offset = Offset(-offset.dx,offset.dy);
    }
    return FractionalTranslation(
      translation: offset,
      transformHitTests: transformHiTests,
      child: child,
    );
  }
}