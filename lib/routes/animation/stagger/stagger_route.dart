
import 'package:flutter/material.dart';

import 'stagger_animation.dart';

/// 交织动画
class StaggerRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StaggerRouteState();
  }
}

class StaggerRouteState extends State<StaggerRoute> with SingleTickerProviderStateMixin {

  late AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("交错动画"),
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: (){
          playAnimation();
        },
        child: Center(
          child: Container(
            width: 300,
            height: 300,
            child: StaggerAnimation(controller: controller),
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.1),
              border: Border.all(
                color: Colors.black.withOpacity(0.5),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<Null> playAnimation() async {
    try {
      // 先正向执行动画
      await controller.forward().orCancel;
      // 然后反向执行动画
      await controller.reverse().orCancel;
    } on TickerCanceled {
      // the animation got canceled, probably because we are disposed
    }
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}