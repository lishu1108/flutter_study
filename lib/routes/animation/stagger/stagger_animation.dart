
import 'package:flutter/material.dart';

class StaggerAnimation extends StatelessWidget {

  StaggerAnimation({required this.controller}){

    // 高度动画
    height = Tween<double>(
      begin: 0,
      end: 300,
    ).animate(
      CurvedAnimation(
        parent: controller,
        curve: Interval(
          0.0,0.6,  //前 60% 的动画时间
          curve: Curves.ease,
        ),
      )
    );

    // 颜色动画
    color = ColorTween(
      begin: Colors.green,
      end: Colors.red,
    ).animate(
      CurvedAnimation(
        parent: controller,
        curve: Interval(
          0.0,0.6,  // 前 60% 的动画时间
          curve: Curves.ease,
        )
      )
    );

    // 平移动画
    padding = Tween<EdgeInsets>(
      begin: EdgeInsets.only(left: 0),
      end: EdgeInsets.only(left: 100),
    ).animate(
      CurvedAnimation(
        parent: controller,
        curve: Interval(
          0.6,1.0,  //  间隔，后 40% 的动画时间
          curve: Curves.ease,
        ),
      )
    );
  }


  final Animation<double> controller;
  late final Animation<double> height;
  late final Animation<EdgeInsets> padding;
  late final Animation<Color?> color;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller,
      builder: _buildAnimation,
    );
  }

  Widget _buildAnimation(BuildContext context, Widget? child){
    return Container(
      alignment: Alignment.bottomCenter,
      padding: padding.value,
      child: Container(
        color: color.value,
        width: 50,
        height: height.value,
      ),
    );
  }

}