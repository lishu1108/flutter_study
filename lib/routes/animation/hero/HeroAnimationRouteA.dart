
import 'package:flutter/material.dart';

import 'HeroAnimationRouteB.dart';

class HeroAnimationRouteA extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("缩略图"),
      ),
      body: Container(
        alignment: Alignment.topCenter,
        child: InkWell(
          child: Hero(
            tag: "avatar",
            child: ClipOval(
              child: Image.asset("assets/images/teacher.jpg",width: 200,height: 200),
            ),
          ),
          onTap: (){
            Navigator.push(context,PageRouteBuilder(pageBuilder: (context,animation,secondaryAnimation){
              return FadeTransition(
                opacity: animation,
                child: HeroAnimationRouteB(),
              );
            }));
          },
        ),
      ),
    );
  }
}