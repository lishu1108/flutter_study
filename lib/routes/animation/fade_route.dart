
import 'package:flutter/material.dart';


/// 自定义路由切换动画
class FadeRoute extends PageRoute {

  FadeRoute({
    required this.builder,
    this.maintainState = true,
    this.transitionDuration = const Duration(microseconds: 300),
    this.barrierColor,
    this.barrierLabel,
  });

  final WidgetBuilder builder;

  @override
  bool maintainState;

  @override
  Duration transitionDuration;

  @override
  Color? barrierColor;

  @override
  String? barrierLabel;


  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
    return builder(context);
  }

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    //当前路由被激活，是打开新路由
    if(isActive) {
      return FadeTransition(
        opacity: animation,
        child: builder(context),
      );
    }else{
      //是返回，则不应用过渡动画
      return Padding(padding: EdgeInsets.zero);
    }
  }
}