
import 'package:flutter/material.dart';

import 'routes.dart';

class HomeRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeRouteState();
  }
}

class HomeRouteState extends State<HomeRoute> {
  Divider divider = Divider(color: Colors.blue);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("首页"),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: ListView.separated(
          separatorBuilder: (context, index) => divider,
          itemCount: Routes.routes.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: (){
                Navigator.pushNamed(context, Routes.routes.keys.elementAt(index));
              },
              child: Container(
                height: 100,
                color: Colors.black54,
                child: Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: Center(
                        child: Text(
                          Routes.routes.keys.elementAt(index),
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 39
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Icon(
                        Icons.chevron_right_rounded,
                        size: 70,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
           );
          },
        ),
      ),
    );
  }
}
