import 'package:flutter/material.dart';

class NamedRoute extends StatelessWidget {
  //final String text;

  //NamedRoute({Key key, @required this.text}): super(key: key);

  @override
  Widget build(BuildContext context) {
    var args = ModalRoute.of(context)?.settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text("Named Route"),
      ),
      body: Center(
        child: Column(
          children: [
            Text("this is Named Route"),
            Text(
              "Receive arguments: ${args?.toString()}",
              style: TextStyle(color: Colors.red),
            )
          ],
        ),
      ),
    );
  }
}