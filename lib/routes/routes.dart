

import 'dart:core';
import 'package:flutter/material.dart';
import 'package:flutter_study/main.dart';
import 'package:flutter_study/routes/ble/ble_route.dart';
import 'animation/scale_animation_route.dart';
import 'animation/hero/HeroAnimationRouteA.dart';
import 'animation/stagger/stagger_route.dart';
import 'animation/switcher/animated_switcher_counter_route.dart';
import 'package_plugin/battery_route.dart';
import 'rive/rive_route.dart';
import 'undefined/named_route.dart';
import 'widget/basic_widget/align_route.dart';
import 'widget/basic_widget/flex_route.dart';
import 'widget/basic_widget/rich_text_route.dart';
import 'widget/basic_widget/slider_route.dart';
import 'widget/basic_widget/stack_positioned_route.dart';
import 'widget/basic_widget/wrap_route.dart';
import 'widget/container_widget/clip_route.dart';
import 'widget/container_widget/container_route.dart';
import 'widget/container_widget/decorated_box_route.dart';
import 'widget/container_widget/padding_route.dart';
import 'widget/container_widget/rotated_box_route.dart';
import 'widget/container_widget/transform_route.dart';
import 'widget/custom/my_countdown_route.dart';
import 'widget/custom/my_parabola_route.dart';
import 'widget/dialog/my_dialog_route.dart';
import 'widget/inherited/inherited_test_route.dart';
import 'widget/list/listview_route.dart';

/// 路由表
class Routes {
  static const String index = "/";
  static const String namedRoute = "namedRoute";
  static const String riveRoute = "riveRoute";
  static const String bleRoute = "bleRoute";
  static const String sliderRoute = "sliderRoute";
  static const String flexRoute = "flexRoute";
  static const String wrapRoute = "wrapRoute";
  static const String stackPositionedRoute = "stackPositionedRoute";
  static const String alignRoute = "alignRoute";
  static const String richTextRoute = "richTextRoute";
  static const String paddingRoute = "paddingRoute";
  static const String decoratedBoxRoute = "decoratedBoxRoute";
  static const String transformRoute = "transformRoute";
  static const String rotatedBoxRoute = "rotatedBoxRoute";
  static const String clipRoute = "clipRoute";
  static const String containerRoute = "containerRoute";
  static const String myDialogRoute = "myDialogRoute";
  static const String listViewRoute = "listViewRoute";
  static const String myCountdownRoute = "myCountdownRoute";
  static const String myParabolaRoute = "myParabolaRoute";
  static const String batteryRoute = "batteryRoute";
  static const String scaleAnimationRoute = "scaleAnimationRoute";
  static const String heroAnimationRouteA = "heroAnimationRouteA";
  static const String staggerRoute = "staggerRoute";
  static const String animatedSwitcherCounterRoute = "animatedSwitcherCounterRoute";
  static const String inheritedTestRoute = "inheritedTestRoute";

  static Map<String,Widget Function(BuildContext context)> routes =
  {}..addAll(indexRoutes)
    ..addAll(undefinedRoutes)
    ..addAll(basicWidgetRoutes)
    ..addAll(containerWidgetRoutes)
    ..addAll(functionWidgetRoutes)
    ..addAll(listViewRoutes)
    ..addAll(customWidgetRoutes)
    ..addAll(packagePluginRoutes)
    ..addAll(animationRoutes);

  /// 首页路由
  static Map<String, Widget Function(BuildContext context)> indexRoutes = {
    index: (context) => MyHomePage(), // 注册首页路由
  };

  /// 没有明确的分类
  static Map<String, Widget Function(BuildContext context)> undefinedRoutes = {
    bleRoute: (context) => BleRoute(),
    riveRoute: (context) => RiveRoute(),
    namedRoute: (context) => NamedRoute(),
  };

  /// 基础 Widget
  static Map<String, Widget Function(BuildContext context)> basicWidgetRoutes = {
    sliderRoute: (context) => SliderRoute(),
    flexRoute: (context) => FlexRoute(),
    wrapRoute: (context) => WrapRoute(),
    stackPositionedRoute: (context) => StackPositionedRoute(),
    alignRoute: (context) => AlignRoute(),
    richTextRoute: (context) => RichTextRoute(),
  };

  /// 容器 Widget
  static Map<String, Widget Function(BuildContext context)> containerWidgetRoutes = {
    paddingRoute: (context) => PaddingRoute(),
    decoratedBoxRoute: (context) => DecoratedBoxRoute(),
    transformRoute: (context) => TransformRoute(),
    rotatedBoxRoute: (context) => RotatedBoxRoute(),
    clipRoute: (context) => ClipRoute(),
    containerRoute: (context) => ContainerRoute(),
  };

  /// 功能性组件
  static Map<String, Widget Function(BuildContext context)> functionWidgetRoutes = {
    myDialogRoute: (context) => MyDialogRoute(),
    inheritedTestRoute: (context) => InheritedTestRoute(),
  };

  /// 列表组件
  static Map<String, Widget Function(BuildContext context)> listViewRoutes = {
    listViewRoute: (context) => ListViewRoute(),
  };

  /// 自定义 Widget
  static Map<String, Widget Function(BuildContext context)> customWidgetRoutes = {
    myCountdownRoute: (context) => MyCountdownRoute(),
    myParabolaRoute: (context) => MyParabolaRoute(),
  };

  /// 包与插件 package_plugin
  static Map<String, Widget Function(BuildContext context)> packagePluginRoutes = {
    batteryRoute: (context) => BatteryRoute(),
  };

  /// 动画
  static Map<String, Widget Function(BuildContext context)> animationRoutes = {
    scaleAnimationRoute: (context) => ScaleAnimationRoute(),
    heroAnimationRouteA: (context) => HeroAnimationRouteA(),
    staggerRoute : (context) => StaggerRoute(),
    animatedSwitcherCounterRoute: (context) => AnimatedSwitcherCounterRoute(),
  };

}