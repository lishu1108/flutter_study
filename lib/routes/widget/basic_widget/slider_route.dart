import 'package:flutter/material.dart';

class SliderRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SliderRouteState();
  }

}

class SliderRouteState extends State<SliderRoute> {
  double _value = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Slider Widget"),
        centerTitle: true,
      ),
      body: RotatedBox(
        quarterTurns: 3,
        child: SliderTheme(
          data: SliderTheme.of(context).copyWith(
            trackHeight: 10, // 轨道高度
            trackShape: FullWidthTrackShape(), // 轨道形状，可以自定义
            activeTrackColor: Colors.blueGrey, // 激活的轨道颜色
            inactiveTrackColor: Colors.black26,  // 未激活的轨道颜色
            thumbShape: RoundSliderThumbShape( //  滑块形状，可以自定义
                enabledThumbRadius: 30  // 滑块大小
            ),
            thumbColor: Colors.white, // 滑块颜色
            overlayShape: RoundSliderOverlayShape(  // 滑块外圈形状，可以自定义
              overlayRadius: 50, // 滑块外圈大小
            ),
            overlayColor: Colors.black54, // 滑块外圈颜色
            valueIndicatorShape: PaddleSliderValueIndicatorShape(), // 标签形状，可以自定义
            activeTickMarkColor: Colors.red,  // 激活的刻度颜色
          ),
          child: Slider(
            value: _value,
            min: 0,
            max: 100,
            divisions: 10,
            label: '$_value',
            onChanged: (v){
              setState(() {
                _value = v;
              });
            },
          ),
        ),
      )
    );
  }
}

class FullWidthTrackShape extends RoundedRectSliderTrackShape {
  Rect getPreferredRect({
    required RenderBox parentBox,
    Offset offset = Offset.zero,
    required SliderThemeData sliderTheme,
    bool isEnabled = false,
    bool isDiscrete = false,
  }) {
    final double? trackHeight = sliderTheme.trackHeight;
    final double trackLeft = offset.dx;
    final double trackTop = offset.dy + (parentBox.size.height - trackHeight!) / 2;
    final double trackWidth = parentBox.size.width;
    return Rect.fromLTWH(trackLeft, trackTop, trackWidth, trackHeight);
  }
}