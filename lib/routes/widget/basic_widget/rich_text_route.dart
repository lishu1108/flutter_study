import 'package:flutter/material.dart';

class RichTextRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Center(
              child: RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: "aaa\n",
                      style: TextStyle(
                          color: Colors.red
                      ),
                    ),
                    TextSpan(
                      text: "bbb",
                      style: TextStyle(
                          color: Colors.green
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}