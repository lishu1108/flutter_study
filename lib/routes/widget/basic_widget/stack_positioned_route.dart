

import 'package:flutter/material.dart';

/// 层叠布局
class StackPositionedRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Stack 层叠布局"),
      ),
      // 通过ConstrainedBox来确保Stack占满屏幕
      body: ConstrainedBox(
        constraints: BoxConstraints.expand(),
        child: Stack(
          alignment: Alignment.center,  // 指定未定位(未使用 Positioned)或部分定位widget的对齐方式
          children: [
            Container(
              child: Text("Hello World",style: TextStyle(color: Colors.white),),
              color: Colors.red,
            ),
            Positioned( // 部分定位
              left: 18.0,
              child: Text("I am Jack"),
            ),
            Positioned( // 部分定位
              top: 18.0,
              child: Text("Your friend"),
            ),
          ],
        ),
      ),
    );
  }
}