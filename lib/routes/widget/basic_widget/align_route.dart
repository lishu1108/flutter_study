
import 'package:flutter/material.dart';

/// Align 组件可以调整子组件的位置，并且可以根据子组件的宽高来确定自身的的宽高
class AlignRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Align 对齐布局"),
      ),
      body: Column(
        children: [
          Container(  // Align 需要放在 Container 中, 若直接放在 Column 或 Row 将会失效
            width: 120,
            height: 120,
            color: Colors.blue[50],
            child: Align(
              alignment: Alignment.topRight,
              child: FlutterLogo(
                size: 60,
              ),
            ),
          ),

        ],
      ),
    );
  }
}