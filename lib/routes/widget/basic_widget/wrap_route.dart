
import 'package:flutter/material.dart';

/// 流式布局
class WrapRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Wrap 流式布局"),
      ),
      body: Wrap(
        alignment: WrapAlignment.center,  // 沿主轴方向居中
        spacing: 8, // 主轴方向间距
        runSpacing: 4,  // 纵轴方向间距
        children: [
          Chip(
            avatar: CircleAvatar(
              backgroundColor: Colors.blue,
              child: Text("A"),
            ),
            label: Text("Hamilton"),
          ),
          Chip(
            avatar: CircleAvatar(
              backgroundColor: Colors.blue,
              child: Text("M"),
            ),
            label: Text("Lafayette"),
          ),
          Chip(
            avatar: CircleAvatar(
              backgroundColor: Colors.blue,
              child: Text("H"),
            ),
            label: Text("Mulligan"),
          ),
          Chip(
            avatar: CircleAvatar(
              backgroundColor: Colors.blue,
              child: Text("J"),
            ),
            label: Text("Laurens"),
          ),
        ],
      ),
    );
  }
}