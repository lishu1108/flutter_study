import 'package:flutter/material.dart';

import '../../../util/log.dart';

typedef OnValueChange = void Function(String val);
class MyListView2 extends StatefulWidget {
  final String defVal;
  final OnValueChange? onValueChange;

  MyListView2(this.defVal, this.onValueChange);

  @override
  _MyListViewState createState() => _MyListViewState();
}

class _MyListViewState extends State<MyListView2> {
  final List<String> items = List.generate(60, (index){
    return (index+1).toString();
  });
  final ScrollController controller = ScrollController();
  int selectedIndex = 4;
  double itemHeight = 50;
  double unitPaddingLeft = 0;

  @override
  void initState() {
    super.initState();
    selectedIndex = items.indexOf(widget.defVal);
    unitPaddingLeft = getPaddingLeft(items[selectedIndex]);
    Future.delayed(Duration.zero, (){
      controller.jumpTo(selectedIndex*itemHeight);
    }).then((val){
      controller.addListener(_scrollListener);
    });
  }

  @override
  void dispose() {
    controller.removeListener(_scrollListener);
    super.dispose();
  }

  void _scrollListener() {
    setState(() {
      int index  = (controller.offset/itemHeight).round();
      if(index != selectedIndex) {
        selectedIndex = index;
        widget.onValueChange!(items[selectedIndex]);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            height: 50,
            decoration: ShapeDecoration(
              color: Color(0xFFF5F5F4),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12)),
              ),
            ),
            child: Center(
              child: AnimatedPadding(
                duration: Duration(milliseconds: 100),
                padding: EdgeInsets.only(left: unitPaddingLeft, top: 5),
                child: Text(
                  "min",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          NotificationListener<ScrollEndNotification>(
            onNotification: (notification){
              Log.i("metrics.pixels: ${notification.metrics.pixels}");
              Log.i("metrics.maxScrollExtent: ${notification.metrics.maxScrollExtent}");
              Log.i("metrics.extentBefore: ${notification.metrics.extentBefore}");
              Log.i("metrics.extentAfter: ${notification.metrics.extentAfter}");
              Log.i("metrics.atEdge: ${notification.metrics.atEdge}");
              Future.delayed(Duration.zero,(){
                controller.animateTo(
                  selectedIndex*itemHeight,
                  duration: Duration(milliseconds: 100),
                  curve: Curves.decelerate,
                );
              });
              return false;
            },
            child: ListView.builder(
              padding: EdgeInsets.only(top: 50, bottom: 50),
              controller: controller,
              itemCount: items.length,
              itemExtent: itemHeight,
              itemBuilder: (context, index) {
                unitPaddingLeft = getPaddingLeft(items[selectedIndex]);
                final isSelected = index == selectedIndex;
                return Container(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        items[index],
                        style: TextStyle(
                          fontSize: isSelected ? 32 : 20,
                          color: isSelected ? Colors.black : Colors.grey,
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  double getPaddingLeft(String item) {
    final textStyle = TextStyle(fontSize: 32, fontWeight: FontWeight.w500);
    final textSpan = TextSpan(text: item, style: textStyle);
    final textPainter = TextPainter(text: textSpan, textDirection: TextDirection.ltr);
    textPainter.layout();
    return textPainter.width + 42;
  }
}
