import 'package:flutter/material.dart';

import '../../../util/log.dart';
import 'my_listview.dart';
import 'my_listview2.dart';

class ListViewRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ListViewRouteState();
  }
}

class ListViewRouteState extends State<ListViewRoute> {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Column(
            children: [
              TextButton(
                child: Text("选中改变背景颜色的 listview"),
                onPressed: (){
                  _showDialog1();
                },
              ),
              TextButton(
                child: Text("中间行高亮 listview"),
                onPressed: (){
                  _showDialog2();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showDialog1() {
    showDialog(
      context: context,
      builder: (ctx){
        return Dialog(
          child: MyListView(),
        );
      },
    );
  }

  void _showDialog2() {
    showDialog(
      context: context,
      builder: (ctx){
        return Dialog(
          insetPadding: EdgeInsets.zero,
          child: MyListView2("4", (val){
            Log.i("val: $val");
          }),
        );
      },
    );
  }
}