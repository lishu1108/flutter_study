import 'package:flutter/material.dart';

///  一个通用的InheritedWidget，保存需要跨组件共享的状态
class InheritedProvider<T> extends InheritedWidget {
  final T data;

  InheritedProvider({required this.data, required Widget child})
      : super(child: child);

  /// 在此简单返回true，则每次更新都会调用依赖其的子孙节点的`didChangeDependencies`。
  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) {
    return true;
  }

}