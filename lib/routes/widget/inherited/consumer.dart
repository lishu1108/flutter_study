import 'package:flutter/material.dart';

import 'change_notifier_provider.dart';

class Consumer<T> extends StatelessWidget {
  final Widget Function(BuildContext context, T? value) builder;

  Consumer({Key? key, required this.builder}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return builder(
      context,
      ChangeNotifierProvider.of<T>(context),
    );
  }

}