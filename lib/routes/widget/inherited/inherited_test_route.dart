import 'package:flutter/material.dart';

import 'inherited_test_widget.dart';
import 'share_data_widget.dart';

class InheritedTestRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InheritedTestRouteState();
  }

}

class InheritedTestRouteState extends State<InheritedTestRoute> {
  int count = 0;

  @override
  Widget build(BuildContext context) {
    return  Center(
      child: ShareDataWidget( //使用ShareDataWidget
        data: count,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: InheritedTestWidget(),//子widget中依赖ShareDataWidget
            ),
            ElevatedButton(
              child: Text("Increment"),
              //每点击一次，将count自增，然后重新build,ShareDataWidget的data将被更新
              onPressed: () => setState(() => ++count),
            )
          ],
        ),
      ),
    );
  }
}