import 'package:flutter/material.dart';

import '../../../util/log.dart';
import 'share_data_widget.dart';

class InheritedTestWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InheritedTestWidgetState();
  }
}

class InheritedTestWidgetState extends State<InheritedTestWidget> {
  @override
  Widget build(BuildContext context) {
    return Text(ShareDataWidget.of(context)!.data.toString());
  }

  //父或祖先widget中的InheritedWidget改变(updateShouldNotify返回true)时会被调用。
  //如果build中没有依赖InheritedWidget，则此回调不会被调用。
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    Log.i("Dependencies change");
  }
}