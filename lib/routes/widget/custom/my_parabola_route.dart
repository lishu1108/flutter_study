import 'package:flutter/material.dart';
import 'package:flutter_study/util/log.dart';

import 'red_dot_widget.dart';

/// 加入购物车效果的抛物线动画
///
class MyParabolaRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyParabolaRouteState();
  }
}

class MyParabolaRouteState extends State<MyParabolaRoute> {
  List<String> options = ["选项1", "选项2", "选项3", "选项4", "选项5", "选项6",
    "选项7", "选项8", "选项1", "选项2", "选项3", "选项4", "选项5", "选项6", "选项7", "选项8"];

  // 终点控件的 key
  GlobalKey _anchorKey = GlobalKey();

  Offset? _anchorOffset;
  // 终点控件左上角坐标
  double? dx, dy;

  @override
  void initState() {
    super.initState();
    // 在控件渲染完成后执行的回调
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      var offset = (_anchorKey.currentContext?.findRenderObject() as RenderBox).localToGlobal(Offset.zero);
      _anchorOffset = offset;
      dx = offset.dx;
      dy = offset.dy;
      Log.i("终点控件的坐标:dx=$dx, dy=$dy");
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Expanded(
              child: ListView.builder(  // 列表
                itemCount: options.length,
                itemBuilder: (context, index){
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(options[index]),
                      Builder(  // 为什么要用 Builder 的 context
                        builder: (context){
                          return IconButton(
                            icon: Icon(Icons.add),
                            onPressed: (){
                              // 点击的时候获取当前 widget 的位置，使用 Overlay 添加一个子 view 显示在最上层
                              OverlayEntry? _overlayEntry = OverlayEntry(builder: (_) {
                                var offset = (context.findRenderObject() as RenderBox).localToGlobal(Offset.zero);
                                return RedDotWidget(
                                  startPosition: offset,
                                  endPosition: _anchorOffset,
                                );
                              });
                              // 显示Overlay
                              Overlay.of(context).insert(_overlayEntry);
                              // 等待动画结束
                              Future.delayed(Duration(milliseconds: 800), () {
                                _overlayEntry?.remove();
                                _overlayEntry = null;
                              });
                            },
                          );
                        },
                      ),
                    ],
                  );
                },
              ),
            ),
            Divider(height: 1), // 分割线
            Container(
              height: 60,
              color: Colors.white,
              padding: EdgeInsets.only(left: 20),
              child: Row(
                children: [
                  Icon(Icons.shop, key: _anchorKey),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

