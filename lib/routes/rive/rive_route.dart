import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

class RiveRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RiveRouteState();
  }
}

class RiveRouteState extends State<RiveRoute> {
  RiveFile? riveFile;
  @override
  void initState() {
    super.initState();
    initData();
  }
  void initData() async {
    riveFile = await RiveFile.asset("assets/images/rive/dinosaur.riv");
    setState(() {
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Rive 动画"),
      ),
      body: Center(
        child: riveFile == null ? SizedBox.shrink() : RiveAnimation.direct(riveFile!),
      ),
    );
  }

}
