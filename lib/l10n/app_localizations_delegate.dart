import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_study/util/log.dart';

import 'app_localizations.dart';

// Locale 代理类
class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  // 是否支持某个 Locale
  @override
  bool isSupported(Locale locale) {
    return ["en", "zh"].contains(locale.languageCode);
  }

  // flutter 会调用此类加载相应的 Locale 资源类
  @override
  Future<AppLocalizations> load(Locale locale) {
    Log.i("load locale: $locale");
    return SynchronousFuture<AppLocalizations>(
      AppLocalizations(locale.languageCode == "zh")
    );
  }

  @override
  bool shouldReload(covariant LocalizationsDelegate<dynamic> old) {
    return false;
  }
}