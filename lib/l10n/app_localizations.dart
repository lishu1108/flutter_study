import 'package:flutter/cupertino.dart';

class AppLocalizations {
  bool isZh = false;

  AppLocalizations(this.isZh);

  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  String get title {
    return isZh ? "Flutter 学习" : "Flutter Study";
  }
}