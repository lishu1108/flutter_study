import 'package:flutter/widgets.dart';

class Person with ChangeNotifier {
  String name = "ChangeNotifierProvider";

  void changeName({required String name}){
    this.name = name;
    notifyListeners();
  }
}
