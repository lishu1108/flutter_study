// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Result<T> _$ResultFromJson<T>(Map<String, dynamic> json) => Result<T>(
      json['code'] as int,
      json['msg'] as String,
      Result._dataFromJson(json['data'] as Object),
    );
