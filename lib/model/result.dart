
import 'package:json_annotation/json_annotation.dart';
part 'result.g.dart';

@JsonSerializable(createToJson: false)
class Result<T> {
  int code;
  String msg;
  @JsonKey(fromJson: _dataFromJson)
  T? data;

  Result(this.code, this.msg, this.data);

  factory Result.fromJson(Map<String, dynamic> json) => _$ResultFromJson(json);

  static T _dataFromJson<T>(Object json) {
    if(json is Map<String, dynamic>) {
      return Result.fromJson(json) as T;
    }

   throw ArgumentError.value(json, 'json', 'Cannot convert the provided data.');
  }
}