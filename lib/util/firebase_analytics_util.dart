import 'package:firebase_analytics/firebase_analytics.dart';

class FirebaseAnalyticsUtil {
  static FirebaseAnalytics _firebaseAnalytics = FirebaseAnalytics.instance;

  static void logEvent({required String name, Map<String, Object?>? parameters,
    AnalyticsCallOptions? callOptions}){
    _firebaseAnalytics.logEvent(name: name, parameters: parameters, callOptions: callOptions);
  }
}